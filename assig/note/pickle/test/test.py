#!/usr/bin/env python
import sys, pickle
sys.path.append('/home/poigal/assig/note/pickle/src')
import temp
import copy
try:
    import cPickle as pickle
except:
    import pickle
import pprint
import os.path

def run_note_tests():
    def test_new_note():
        b=copy.deepcopy(temp.mlist)        
        temp.new_note("hello","world")
        assert(temp.mlist!=b)
        temp.delete_note("hello")
        
    def test_find_note():
        a=[]
        temp.new_note("hello","world")
        temp.new_note("helluu","world")
        temp.new_note("helluu","world")    
        a=temp.find_note("helluu")
        assert(len(a)==2)
        temp.delete_note("hello")
        temp.delete_note("helluu")
        temp.delete_note("helluu")
               
    def test_save_note(): 
        temp.new_note("hello","world")   
        temp.save_note('/home/poigal/assig/note/pickle/test/')
        file2 = open('/home/poigal/assig/note/pickle/test/picklefile.txt', "r+")
        file2.seek(0)
        a=pickle.loads(file2.read())
        assert(a==temp.mlist)  
        temp.delete_note("hello")
        file2.close()

        
        
    test_new_note()
    test_find_note()
    test_save_note()
    
    
run_note_tests()
