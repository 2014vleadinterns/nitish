#!/usr/bin/env python
import sys
import json
import os.path

__all__ = ["new_note", "delete_note", "find_note","save_note"]

mlist = []
    
def new_note(title,body):
    global mlist
    mlist.append((title,body))
    print mlist[len(mlist)-1],"<--added new note"


def find_note(key):
    global mlist    
    temp_index=[]
    for i in range(len(mlist)):
        if mlist[i][0].find(key)>=0 or mlist[i][1].find(key)>=0:            
            temp_index.append(i)   
    for j in temp_index:
        print mlist[j],
    print "<--found notes"
    return temp_index           

def delete_note(title):
    global mlist
    for i in range(len(mlist)):
        if mlist[i][0].find(title)>=0 or mlist[i][1].find(title)>=0:            
            print mlist[i],"<--deleted note"
            mlist.pop(i)            
            break

def save_note(path):
    global mlist
    completeName = os.path.join(path, "json.txt")
    file1 = open(completeName, "w+")
    file1.write(json.dumps(mlist))        
    file1.close()





