#!/usr/bin/env python
import sys
sys.path.append('/home/poigal/assig/note/xml/src')
import temp
import copy, os.path


def run_note_tests():
    def test_new_note():
        b=copy.deepcopy(temp.mlist)        
        temp.new_note("hello","world")
        assert(temp.mlist!=b)
        temp.delete_note("hello")
        
    def test_find_note():
        a=[]
        temp.new_note("hello","world")
        temp.new_note("helluu","world")
        temp.new_note("helluu","world")    
        a=temp.find_note("helluu")
        assert(len(a)==2)
        temp.delete_note("hello")
        temp.delete_note("helluu")
        temp.delete_note("helluu")
        
    def test_save_note(): 
        temp.new_note("hello","world")
        temp.new_note("my","name")
        temp.new_note("is","nitish") 
        temp.save_note('/home/poigal/assig/note/xml/test/')
        temp.delete_note("hello")
        temp.delete_note("my")
        temp.delete_note("is") 
        assert(os.path.exists('/home/poigal/assig/note/xml/test/notes.txt'))
               


        
        
    test_new_note()
    test_find_note()
    test_save_note()
    
    
run_note_tests()
