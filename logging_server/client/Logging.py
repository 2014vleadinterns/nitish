import os
import os.path
import logging
from logging.handlers import HTTPHandler
import json

config_spec = json.loads(open("../client/config.json").read())
log_hostname = config_spec["LOGGING_CONFIGURATION"]["SERVER_HOSTNAME"]
log_port = config_spec["LOGGING_CONFIGURATION"]["SERVER_PORT"]
log_endpoint = config_spec["API_ENDPOINTS"]["LOGGING_URI_ENDPOINT"]

def get_logger(name, level):
    name = name
    level = level
    logger = logging.getLogger(name)
    logger.setLevel(level)
    if logger.handlers == []:
        http_handler = HTTPHandler(host=log_hostname+':'+log_port,
                                   url=log_endpoint, method="POST")
        logger.addHandler(http_handler)
    return logger
