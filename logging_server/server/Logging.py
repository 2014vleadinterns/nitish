# logging imports
import logging
import logging.handlers
# other impors
import json
import os

used_loggers = {}


def get_logger(name):
    """Returns logger object"""
    logger = None
    if name in used_loggers:
        logger = used_loggers[name]
    else:
        logger = logging.getLogger(name)
        used_loggers[name] = logger
        LOG_FILE_PATH = './log/' + str(name) + '.log'
        timed_handler = logging.handlers.TimedRotatingFileHandler(
            LOG_FILE_PATH, when='midnight', backupCount=5)
        formatter = logging.Formatter('%(asctime)s: %(message)s',
                                      datefmt='%m/%d/%Y %I:%M:%S %p')
        timed_handler.setFormatter(formatter)
        logger.addHandler(timed_handler)
        logger.propagate = False
        logger.has_handlers = True
    return logger


def log(arguments):
    """Creates an instance of a LogRecord object and \
    Handles the record by passing it to all handlers \
    associated with this logger"""

    levelname = arguments['levelname'][0]
    pathname = arguments['pathname'][0]
    lineno = arguments['lineno'][0]
    name = arguments['name'][0]
    message = arguments['msg'][0]
    funcName = arguments['funcName'][0]
    logger = get_logger(name)
    fmt_string = "%(levelname)-8s|%(pathname)s:%(lineno)-8s|%(name)s\
        \t-> %(message)s"
    record_format_args = {
        'levelname': levelname,
        'pathname': pathname,
        'lineno': lineno,
        'name': name,
        'message': message
    }
    record = logger.makeRecord(name, levelname, funcName, lineno,
                               fmt_string, record_format_args, None)
    logger.handle(record)


if not os.path.isdir("log"):
    os.mkdir("log")
